var restler        = require('restler');

exports.getComments = function(target, callback, comments, after) {
  console.log('getComments(' + target + ',callback,comments,'+after);
  if (!comments) { comments = []; }
  var baseUrl = "http://reddit.com/user/"+target+".json?limit=100";
  var url = baseUrl;
  if (after) {
    url = baseUrl + "&after="+after;
  }

  restler.get(url).on('complete',function(data) {
    console.log('restler on complete from ' + url + "after="+after);
    if (!data.data) { 
      console.log('!data.children');
      console.log("comments="+JSON.stringify(comments));
      console.log("data = " + JSON.stringify(data));
      return callback(comments); 
    }
    for (var i = 0; i < data.data.children.length; i++) {
      if (data.data.children[i].data.id) {
        comments.push(data.data.children[i].data.id);
      } else {
        console.log("Error: data.children[i].id is undefined. This is bad");
        console.log("Error: data = " + JSON.stringify(data));
      }
    }

    var newAfter = data.data.after;
    console.log('newAfter = ' + newAfter);

    if (newAfter) {
      console.log('about to getCommen ts again! newAfter = ' + newAfter);
      setTimeout( function() {
        return exports.getComments(target,callback,comments,data.data.after);
      },300);
    } else {
      return callback(comments);
    }
  });
}

exports.voteOnComment = function(commentId, direction, accessToken, callback) {
  var url = "https://oauth.reddit.com/api/vote";
  console.log("voteOnComment url = " + url);
  console.log("voteOnComment accessToken = " + accessToken);
  restler.postJson(url, {
      data: { 
        id: commentId,
        direction: direction
      }
    }, { 
      headers: { "Authorization": "Bearer " + accessToken } 
    }
  ).on('complete', function(result, response) {
    console.log('voteOnComment complete!');
    console.log('voteOnComment complete result = ' + JSON.stringify(result));
    console.log('voteOnComment complete response = ' + Object.keys(response));
    console.log('voteOnComment complete response.headers = ' + JSON.stringify(response.headers));
    console.log('voteOnComment complete response.statusCode = ' + response.statusCode);
    return callback();
  });
}







