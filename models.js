exports.configure = function(db) {
  var PostSchema = new db.Schema({
    "name": String,
    "author": String,
    "permalink": String,
    "url": String,
    "imageUrl": String,
    "localImageUrl": String,
    "created": String
  });

  exports.Post = db.model('Post', PostSchema);
};
