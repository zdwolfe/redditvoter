var express        = require('express')
  , passport       = require('passport')
  , util           = require('util')
  , reddit         = require('./reddit')
  , RedditStrategy = require('passport-reddit').Strategy
  , http           = require('http')
  , crypto         = require('crypto')
  , config         = require('./config')
  , restler        = require('restler');

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});


passport.use(new RedditStrategy({
    clientID: config.reddit.consumerKey,
    clientSecret: config.reddit.consumerSecret,
    callbackURL: config.reddit.callbackUrl
  },
  function(accessToken, refreshToken, profile, done) {
    console.log('RedditStrategy accessToken = ' + accessToken);
    // asynchronous verification, for effect...
    process.nextTick(function () {
      profile.accessToken = accessToken;
      return done(null, profile);
    });
  }
));



var app = express();

// configure Express
app.configure(function() {
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.logger());
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.session({ secret: 'keyboard cat' }));
  // Initialize Passport!  Also use passport.session() middleware, to support
  // persistent login sessions (recommended).
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});


app.get('/', function(req, res){
  res.render('index', { user: req.user });
});

app.get('/comments/:targetUsername', function(req, res){
  var target = req.params.targetUsername;
  if (!target) {
    res.status(400);
    return res.end();
  }
  return reddit.getComments(target,function(comments) { 
    return res.json(comments); 
  });
});


app.post('/vote', ensureAuthenticated, function(req, res) {
  var accessToken = req.user.accessToken;
  console.log('/vote/'+req.body.targetCommentId + " accessToken=" + accessToken);
  if (!req.body || !req.body.direction || !req.body.targetCommentId) {
    res.status(400);
    return res.end();
  }
  return reddit.voteOnComment(req.body.targetCommentId, req.body.direction,
    accessToken,function() {
      res.status(200);
      return res.end();
  });
});

app.get('/vote', ensureAuthenticated, function(req, res) {
  return res.render("vote");
});

app.get('/account', ensureAuthenticated, function(req, res){
  console.log('req.user = ' + JSON.stringify(req.user));
  res.render('account', { user: req.user });
});

app.get('/login', function(req, res){
  res.render('login', { user: req.user });
});

app.get('/auth/reddit', function(req, res, next){
  req.session.state = crypto.randomBytes(32).toString('hex');
  passport.authenticate('reddit', {
    scope: 'identity,vote',
    state: req.session.state,
  })(req, res, next);
});

app.get('/auth/reddit/callback', function(req, res, next){
  // Check for origin via state token
  passport.authenticate('reddit', {
    successRedirect: '/vote',
    failureRedirect: '/login'
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

app.listen(3000);

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login');
}
